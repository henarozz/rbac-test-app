<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use common\models\User;
use backend\models\ServiceSnapshot;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property int $price
 * @property string $discription
 * @property int $status
 * @property string $expired_at
 * @property string $city_in
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Service extends \yii\db\ActiveRecord
{
    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    public static $statuses = [
        self::STATUS_DISABLED => 'Disabled',
        self::STATUS_ENABLED => 'Enabled',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service';
    }

    public function behaviors()
    {
        return array_merge(
            [
                [
                    'class' => TimestampBehavior::className(),
                    'value' => (new \DateTime('now'))->format('Y-m-d H:i:s'),
                ],
                [
                    'class' => BlameableBehavior::className(),
                    'value' => function () {
                        return !isset(Yii::$app->user) ? 1 : Yii::$app->user->id;
                    },
                ]
            ], parent::behaviors()
        );
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $snapshotModel = new ServiceSnapshot();
        $attributes = $this->getAttributes();
        $attributes['service_id'] = $this->id;
        $snapshotModel->setAttributes($attributes);
        $snapshotModel->save();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'city_in'], 'required'],
            [['price', 'status', 'created_by', 'updated_by'], 'integer'],
            [['discription'], 'string'],
            [['expired_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 256],
            [['code'], 'string', 'max' => 32],
            [['city_in'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'price' => 'Price',
            'discription' => 'Discription',
            'status' => 'Status',
            'expired_at' => 'Expired At',
            'city_in' => 'City In',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSnapshots()
    {
        $query = $this->hasMany(ServiceSnapshot::className(), ['service_id' => 'id'])
            ->orderBy('id DESC');

        return $query;
    }
}
