<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "service_snapshot".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property int $price
 * @property string $discription
 * @property int $status
 * @property string $expired_at
 * @property string $city_in
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $service_id
 */
class ServiceSnapshot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_snapshot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'city_in'], 'required'],
            [['price', 'status', 'created_by', 'updated_by', 'service_id'], 'integer'],
            [['discription'], 'string'],
            [['expired_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 256],
            [['code'], 'string', 'max' => 32],
            [['city_in'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'price' => 'Price',
            'discription' => 'Discription',
            'status' => 'Status',
            'expired_at' => 'Expired At',
            'city_in' => 'City In',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'service_id' => 'Service ID',
        ];
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
