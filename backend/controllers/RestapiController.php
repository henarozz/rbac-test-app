<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;

class RestapiController extends \yii\rest\ActiveController
{
    public $modelClass = 'backend\models\Service';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        return $behaviors;
    }

    /**
     * Search a Service model by criteria
     *
     * @return ActiveDataProvider
     * @throws \yii\web\HttpException
     */
    public function actionSearch()
    {
        if (empty(Yii::$app->request->queryParams)) {
            throw new \yii\web\HttpException(400, 'No query string');
        }

        $model = new $this->modelClass;

        foreach (Yii::$app->request->queryParams as $key => $value) {
            if (!$model->hasAttribute($key)) {
                throw new \yii\web\HttpException(404, 'Invalid attribute:' . $key);
            }
        }

        try {
            $provider = new ActiveDataProvider([
                'query' => $model->find()->where(Yii::$app->request->queryParams),
                'pagination' => false
            ]);
        } catch (\Exception $e) {
            throw new \yii\web\HttpException(500, 'Internal server error');
        }

        if ($provider->getCount() <= 0) {
            throw new \yii\web\HttpException(404, 'No entries found with this query string');
        } else {
            return $provider;
        }
    }
}
