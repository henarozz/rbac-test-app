INSTALLATION
------------
    ./init (dev)
    ./composer install

HOME
----

    @backend/web/

CONSOLE
-------
    ./yii rbac/init

USERS
-----
    admin / admin123
    operator / operator123

REST API
--------
Endpoint:

    http://{host:port}/service/restapi

Request headers:

    Accept: application/json
    Authorization: Bearer O3WCjXi3MDHoPQItrikore34v0gaW1Xb_1560431301
    
Methods:

    http://{host:port}/service/restapi                          -- getAll
    http://{host:port}/service/restapi/1                        -- getById
    http://{host:port}/service/restapi/search?city_in=Moscow    -- searchByAttribute


DATABASE
--------
    @backend/sqlite/rbac_test_app.db

CONFIG
------

./common/config/main-local.php
```
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            //'dsn' => 'mysql:host=localhost;dbname=yii2advanced',
            'dsn' => 'sqlite:@backend/sqlite/rbac_test_app.db',
            //'username' => 'root',
            //'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
    ],
];
```

DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```
