<?php

use yii\db\Migration;

/**
 * Class m190613_095309_create_tbl_service_snapshot
 */
class m190613_095309_create_tbl_service_snapshot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('service_snapshot', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(256)->notNull(),
            'code' => $this->string(32),
            'price' => $this->integer(6),
            'discription' => $this->text(),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'expired_at' => $this->dateTime(),
            'city_in' => $this->string(64)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'service_id' => $this->integer(11),
        ]);

        /* внешние ключи не поддерживаются в sqlite
        $this->addForeignKey(
            'fk_service_snapshot_service_id',
            'service_snapshot',
            'service_id',
            'service',
            'id'
        );*/

        $this->createIndex(
            'idx_service_snapshot_service_id',
            'service_snapshot',
            'service_id',
            false
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx_service_snapshot_service_id',
            'service_snapshot'
        );

        /* внешние ключи не поддерживаются в sqlite
        $this->dropForeignKey(
            'fk_service_snapshot_service_id',
            'service_snapshot'
        );*/

        $this->dropTable('service_snapshot');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190613_095309_create_tbl_service_snapshot cannot be reverted.\n";

        return false;
    }
    */
}
