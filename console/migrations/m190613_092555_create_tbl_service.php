<?php

use yii\db\Migration;

/**
 * Class m190613_092555_create_tbl_service
 */
class m190613_092555_create_tbl_service extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('service', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(256)->notNull(),
            'code' => $this->string(32),
            'price' => $this->integer(6),
            'discription' => $this->text(),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'expired_at' => $this->dateTime(),
            'city_in' => $this->string(64)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('service');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190613_092555_create_tbl_service cannot be reverted.\n";

        return false;
    }
    */
}
