<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use common\models\User;

class RbacController extends Controller
{
    public function actionInit()
    {
        $authManager = Yii::$app->authManager;

        // добавляем разрешение <createService>
        $createService = $authManager->createPermission('createService');
        $createService->description = 'Создание услуги (только администраторы)';

        try {
            $authManager->add($createService);
        } catch (\Exception $e) {
            //
        }

        // добавляем разрешение <switchService>
        $switchService = $authManager->createPermission('switchService');
        $switchService->description = 'Включение/выключение услуги (только администраторы)';

        try {
            $authManager->add($switchService);
        } catch (\Exception $e) {
            //
        }

        // добавляем разрешение <updateService>
        $updateService = $authManager->createPermission('updateService');
        $updateService->description = 'Редактирование услуги (все)';

        try {
            $authManager->add($updateService);
        } catch (\Exception $e) {
            //
        }

        // добавляем разрешение <deleteService>
        $deleteService = $authManager->createPermission('deleteService');
        $deleteService->description = 'Удаление услуги (только администраторы)';

        try {
            $authManager->add($deleteService);
        } catch (\Exception $e) {
            //
        }

        // добавляем разрешение <historyService>
        $historyService = $authManager->createPermission('historyService');
        $historyService->description = 'Просмотр истории изменений (все)';

        try {
            $authManager->add($historyService);
        } catch (\Exception $e) {
            //
        }

        // добавляем роль <operator> и даем разрешения: <updateService>, <historyService>
        $operator = $authManager->createRole('operator');

        try {
            $authManager->add($operator);
        } catch (\Exception $e) {
            //
        }

        try {
            $authManager->addChild($operator, $updateService);
        } catch (\Exception $e) {
            //
        }

        try {
            $authManager->addChild($operator, $historyService);
        } catch (\Exception $e) {
            //
        }

        // добавляем роль <admin> и даем разрешения: <switchService>, <createService>, <deleteService>,
        // + наследуем разрешения от роли <operator>
        $admin = $authManager->createRole('admin');

        try {
            $authManager->add($admin);
        } catch (\Exception $e) {
            //
        }

        try {
            $authManager->addChild($admin, $createService);
        } catch (\Exception $e) {
            //
        }

        try {
            $authManager->addChild($admin, $deleteService);
        } catch (\Exception $e) {
            //
        }

        try {
            $authManager->addChild($admin, $switchService);
        } catch (\Exception $e) {
            //
        }

        try {
            $authManager->addChild($admin, $operator);
        } catch (\Exception $e) {
            //
        }

        // проверяем и создаем пользователей при необходимости
        $users = User::find()
            ->select(['id', 'username'])
            ->where(['in', 'id', [1, 2]])
            ->asArray()
            ->all();

        $users = ArrayHelper::index($users, 'id');

        if (!isset($users[1])) {
            $user = new User();
            $user->id = 1;
            $user->username = 'admin';
            $user->email = 'admin@localhost.local';
            $user->setPassword('admin123');
            $user->generateAuthKey();
            $user->generateEmailVerificationToken();
            $user->status = User::STATUS_ACTIVE;
            $user->save();

        }

        if (!isset($users[2])) {
            $user = new User();
            $user->id = 2;
            $user->username = 'operator';
            $user->email = 'operator@localhost.local';
            $user->setPassword('operator123');
            $user->generateAuthKey();
            $user->generateEmailVerificationToken();
            $user->status = User::STATUS_ACTIVE;
            $user->save();

        }

        // назначаем роли дефолтным пользователям: 1 -- admin, 2 -- operator
        try {
            $authManager->assign($operator, 2);
        } catch (\Exception $e) {
            //
        }

        try {
            $authManager->assign($admin, 1);
        } catch (\Exception $e) {
            //
        }

        echo 'Роли и разрешения добавлены' . PHP_EOL;
    }

}
